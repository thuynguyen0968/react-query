import { api, apiSW } from '@/api';

const getPosts = async (page = 1, limit = 5) => {
  const res = await api.get<IPosts[]>(`posts?_page=${page}&_limit=${limit}`);
  return res;
};

const fetchComments = async (postId: number) => {
  const res = await api.get<IComments[]>(`comments?postId=${postId}`);
  return res.data;
};

const deletePost = async (postId: number) => {
  return await api.delete(`posts/${postId}`);
};

const updatePost = async (postId: number, body: IPosts) => {
  const res = await api.patch(`posts/${postId}`, body);
  return res.data;
};

const fetchPeople = async (path: string) => {
  const res = await apiSW.get(path);
  return res.data;
};

export { getPosts, deletePost, updatePost, fetchComments, fetchPeople };
