import { deletePost, fetchComments, updatePost } from '@/services/blog';
import { useMutation, useQuery, useQueryClient } from '@tanstack/react-query';
import { Fragment } from 'react';
import { toast } from 'react-toastify';

type Props = {
  post: IPosts;
};

export default function PostDetail({ post }: Props) {
  const queryClient = useQueryClient();
  const { data, isLoading, isError } = useQuery<IComments[]>({
    queryKey: ['post', post.id],
    queryFn: () => fetchComments(post.id),
  });

  const deleteMutation = useMutation({
    mutationFn: () => deletePost(post.id),
  });

  const updateMutation = useMutation({
    mutationFn: (body: IPosts) => updatePost(post.id, body),
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: ['posts'] });
    },
  });

  const onDelete = () => {
    deleteMutation.mutate();
    toast.success('Delete post successfull!');
  };

  const onUpdate = () => {
    const payload = { ...post, title: 'REACT QUERY FOREVER....' };
    updateMutation.mutate(payload);
    toast.success('Update post successfull!');
  };

  if (isError) return <p>Somthing went wrong, try again</p>;
  if (isLoading) return <p>Loading data....</p>;

  return (
    <Fragment>
      <h3 style={{ color: 'blue' }}>{post.title}</h3>
      <div style={{ display: 'flex', gap: 5 }}>
        <button onClick={onDelete}>Delete</button>
        <button onClick={onUpdate}>Update</button>
      </div>
      <p>{post.body}</p>
      <h4>Comments</h4>
      <Fragment>
        {data &&
          data.map((comment) => (
            <li key={comment.id}>
              <strong>{comment.email}</strong>: {comment.body}
            </li>
          ))}
      </Fragment>
    </Fragment>
  );
}
