import { InfiniteSpecies } from '@/components/Species';
import { Fragment } from 'react';

const HomePage = () => {
  return (
    <Fragment>
      <InfiniteSpecies />
    </Fragment>
  );
};

export default HomePage;
