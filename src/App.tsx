import Toastify from './components/Toasty';
import AppRoutes from './routes';

function App() {
  return (
    <section className='App'>
      <AppRoutes />
      <Toastify />
    </section>
  );
}

export default App;
