interface IPosts {
  id: number;
  title: string;
  body: string;
  userId: number;
}
interface IComments {
  postId: number;
  id: number;
  name: string;
  email: string;
  body: string;
}
