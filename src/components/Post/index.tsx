import PostDetail from '@/Pages/Posts';
import { getPosts } from '@/services/blog';
import { useQuery, useQueryClient } from '@tanstack/react-query';
import { useState, Fragment, useEffect } from 'react';

type Page = 'next' | 'prev';
const LIMIT = 10;

export function Posts() {
  const [page, setPage] = useState<number>(1);
  const [selectedPost, setSelectedPost] = useState<IPosts | null>(null);
  const queryClient = useQueryClient();

  // replace with useQuery
  const { data, isLoading, isError } = useQuery({
    queryKey: ['posts', page],
    queryFn: () => getPosts(page, LIMIT),
    staleTime: 10 * 60 * 1000,
    keepPreviousData: true,
  });

  const posts = data ? [...data.data] : [];
  const totalPage = parseInt(data?.headers['x-total-count']) / LIMIT;

  // prefectching
  useEffect(() => {
    const nextPage = page + 1;
    queryClient.prefetchQuery({
      queryKey: ['posts', nextPage],
      queryFn: () => getPosts(page, LIMIT),
    });
  }, [page, queryClient]);

  if (isError) return <p>Somthing went wrong, try again</p>;
  if (isLoading) return <p>Loading data....</p>;

  const onPageChange = (type: Page) => {
    switch (type) {
      case 'next':
        return setPage(page + 1);

      default:
        return setPage(page - 1);
    }
  };

  return (
    <Fragment>
      <ul>
        <Fragment>
          {posts &&
            posts.map((post) => (
              <li
                key={post.id}
                className='post-title'
                onClick={() => setSelectedPost(post)}
              >
                {post.title}
              </li>
            ))}
        </Fragment>
      </ul>
      <div style={{ display: 'flex', gap: 5 }}>
        <button disabled={page === 1} onClick={() => onPageChange('prev')}>
          Prev
        </button>
        <button
          disabled={page === totalPage}
          onClick={() => onPageChange('next')}
        >
          Next
        </button>
      </div>

      {selectedPost && <PostDetail post={selectedPost} />}
    </Fragment>
  );
}
