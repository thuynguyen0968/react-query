import InfiniteScroll from 'react-infinite-scroller';
import { Person } from './Person';
import { useInfiniteQuery } from '@tanstack/react-query';
import { fetchPeople } from '@/services/blog';

export default function InfinitePeople() {
  const { data, fetchNextPage, hasNextPage, isError, error } = useInfiniteQuery(
    {
      queryKey: ['sw-people'],
      queryFn: ({ pageParam = 'people' }) => fetchPeople(pageParam),
      getNextPageParam: (lastPage) => lastPage.next || undefined,
    }
  );
  if (isError) return <p>Error : {error?.toString()}</p>;
  return (
    <InfiniteScroll loadMore={fetchNextPage as any} hasMore={hasNextPage}>
      {data ? (
        data?.pages.map((pageData) =>
          pageData.results.map((item: any, index: number) => (
            <Person
              key={index}
              name={item.name}
              hairColor={item.hair_color}
              eyeColor={item.eye_color}
            ></Person>
          ))
        )
      ) : (
        <p>Loading...</p>
      )}
    </InfiniteScroll>
  );
}
