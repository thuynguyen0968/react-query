import InfiniteScroll from 'react-infinite-scroller';
import { Species } from './Species';
import { useInfiniteQuery } from '@tanstack/react-query';
import { fetchPeople } from '@/services/blog';

export function InfiniteSpecies() {
  const { data, isLoading, hasNextPage, fetchNextPage } = useInfiniteQuery({
    queryKey: ['sw-species'],
    queryFn: ({ pageParam = 'species' }) => fetchPeople(pageParam),
    getNextPageParam: (lastPage) => lastPage.next || undefined,
  });
  if (isLoading) return <p>Loading...</p>;
  return (
    <InfiniteScroll hasMore={hasNextPage} loadMore={fetchNextPage as any}>
      {data &&
        data.pages.map((pageData) => {
          return pageData.results.map((item: any, index: number) => (
            <Species
              key={index}
              name={item.name}
              averageLifespan={item.average_lifespan}
              language={item.language}
            ></Species>
          ));
        })}
    </InfiniteScroll>
  );
}
