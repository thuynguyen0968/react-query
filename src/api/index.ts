import axios from 'axios';

export const api = axios.create({
  baseURL: import.meta.env.VITE_BASE_API,
  headers: {
    'Content-Type': 'application/json',
  },
});

export const apiSW = axios.create({
  baseURL: import.meta.env.VITE_SWAPI,
  headers: {
    'Content-Type': 'application/json',
  },
});
